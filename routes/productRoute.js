const express = require('express')
const db = require('../db')
const router = express.Router()
const utils = require('../utils')

router.post('/', (request,response) => {
    const {title,description,category,price} = request.body

    const sql = `INSERT INTO product(title,description,category,price) 
                 VALUES('${title}','${description}','${category}',${price})`

    const connection = db.openConnection()
    connection.query(sql, (error,data) =>{
        connection.end()
        response.send(utils.createResponse(error, data))
    })
})


router.put('/:id', (request,response) => {
    const {title,description,category,price} = request.body
    const {id} = request.params
    const sql = `UPDATE product SET 
                title = '${title}',
                description = '${description}',
                category = '${category}',
                price = ${price} 
                WHERE id = ${id}`

    const connection = db.openConnection()
    connection.query(sql, (error,data) =>{
        connection.end()
        response.send(utils.createResponse(error, data))
    })
})


router.get('/:id', (request,response) => {
    const {id} = request.params

    const sql = `SELECT * FROM product WHERE id =${id}`

    const connection = db.openConnection()
    connection.query(sql, (error,data) =>{
        connection.end()
        response.send(utils.createResponse(error, data))
    })
})

router.delete('/:id', (request,response) => {
    const {id} = request.params

    const sql = `DELETE FROM product WHERE id =${id}`

    const connection = db.openConnection()
    connection.query(sql, (error,data) =>{
        connection.end()
        response.send(utils.createResponse(error, data))
    })
})
module.exports = router
