const express = require('express')
const app = express()
const productRouter = require('./routes/productRoute')

app.use(express.json())
app.use('/product', productRouter)


app.listen(4000, () =>{
  console.log("server started on 4000")
})


// In server js
// 1. import express
// 2. create app from express
// 3. import routers
// 4. Use the routers
// 5. Use express.json
// 6. start listening



// In router js
// 1. import express
// 2. import db
// 3. create router from express
// 4. add handlers for get / post / put / delete
// 5. export router 

// In db js
// 1. Import mysql
// 2. Write openConnection function
// 3. Create and return connection
// 4. export { openConnection } 

// In Utils
// 1. createResponse method - 2 params error and data
// 2. Create empty response object
// 3. if error is present, set response.status as error and response.error as given error obj 
// 4. otherwise set response.status as success and response.data as given data