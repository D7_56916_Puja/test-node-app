const createResponse = (error, data) => {
    const response = {}
    if(error) {
        response.status = 'error'
        response.error = error
    }
    else {
        response.status = 'success'
        response.data = data
    }
    return response
}

module.exports = {
    createResponse
}